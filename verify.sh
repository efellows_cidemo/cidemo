#!/bin/sh
set -eux
docker --version
docker run hello-world
docker-compose --version
java --version
mvn --version

./start-infrastructure.sh
docker exec cidemo_selenium-standalone-chrome_1 curl http://cidemo_nginx_1:80
./run-integration-test.sh
./stop-infrastructure.sh
