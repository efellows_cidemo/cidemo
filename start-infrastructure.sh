#!/bin/sh
set -eux
docker-compose -p cidemo -f test-infrastructure-docker-compose.yml up -d
docker cp ./web/* cidemo_nginx_1:/usr/share/nginx/html
