#!/bin/sh
set -eux
cd integration-test || exit
mvn -B verify -Dwebdriver.remote.url=$SELENIUM_REMOTE_URL
cd ..
