# cidemo
Find the deployed version at:
[cidemo.freecluster.eu](http://cidemo.freecluster.eu)

Find the SonarCloud Analysis at [sonarcloud.io/organizations/efellows-cidemo/projects](https://sonarcloud.io/organizations/efellows-cidemo/projects)

Copyright by [Philip Pütsch](philip.puetsch@d-fine.de) and others, [d-fine](https://www.d-fine.de)

have a look a the [d-fine](https://www.d-fine.de) page or contact [info@d-fine.de](info@d-fine.de) for general topics.

You want to become part of the team? Have a look at the [d-fine Carreer](https://www.d-fine.com/karriere/deutschland/uebersicht/) page
