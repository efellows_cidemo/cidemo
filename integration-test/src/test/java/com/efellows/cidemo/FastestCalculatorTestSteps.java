package com.efellows.cidemo;

import net.thucydides.core.annotations.Screenshots;
import net.thucydides.core.annotations.Step;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

@Screenshots
public class FastestCalculatorTestSteps {
    @Step("Given the FastestCalculator page is opened")
    public void openDriver() {
        getDriver().get("http://cidemo_nginx_1:80");
    }

    @Step("Then wait until the Calculate Button is loaded")
    public void waitForCalculateButton() {
        WebDriverWait webDriverWait = new WebDriverWait(getDriver(), 20);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("addBtn")));
    }

    @Step("When two numbers are entered and calucate is pressed")
    public void enterTwoNumbersAndHitCalculate() {
        getDriver().findElement(By.id("n1")).sendKeys("2");
        getDriver().findElement(By.id("n2")).sendKeys("3");
        getDriver().findElement(By.id("addBtn")).click();
    }

    @Step("Then the result should be 5")
    public void checkThatResultIsFive() {
        Assertions.assertThat(getDriver().findElement(By.id("result")).getAttribute("value"))
                .as("Checking that the Result equals 5")
                .isEqualTo("5");
    }
}
