package com.efellows.cidemo;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
@Narrative(text = {"Testing the fastest calculator that adds two numbers in the whole internet"})
@Screenshots
public class FastestCalculatorTest {
    @Managed
    WebDriver browser;

    @Steps
    private FastestCalculatorTestSteps fastestCalculatorTestSteps;

    @Test
    @Title("Checks that the Calculator can be opened")
    public void checkThatTheCalculatorCanBeOpened() {
        fastestCalculatorTestSteps.openDriver();
    }

    @Test
    @Title("Checks that when the Calculator adds 2 and 3 the result is 5")
    public void checkTwoAndThreeEqualsFive() {
        fastestCalculatorTestSteps.openDriver();
        fastestCalculatorTestSteps.waitForCalculateButton();
        fastestCalculatorTestSteps.enterTwoNumbersAndHitCalculate();
        fastestCalculatorTestSteps.checkThatResultIsFive();
    }
}
